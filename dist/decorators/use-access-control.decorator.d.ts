import { AccessControlOptions } from '../interfaces/access-control-options.interface';
/**
 * Define access control filters required for this route.
 */
export declare const UseAccessControl: (options: AccessControlOptions) => import("@nestjs/common").CustomDecorator<string>;
