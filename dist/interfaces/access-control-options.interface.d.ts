import { AccessControlFilterService } from './access-control-filter-service.interface';
export interface AccessControlOptions {
    service: AccessControlFilterService;
}
