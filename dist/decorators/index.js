"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./use-access-control.decorator"));
__export(require("./access-control-create-one.decorator"));
__export(require("./access-control-delete-one.decorator"));
__export(require("./access-control-filter.decorator"));
__export(require("./access-control-grant.decorator"));
__export(require("./access-control-read-many.decorator"));
__export(require("./access-control-read-one.decorator"));
__export(require("./access-control-update-one.decorator"));
__export(require("./inject-access-control.decorator"));
//# sourceMappingURL=index.js.map