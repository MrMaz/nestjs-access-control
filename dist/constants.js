"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ACCESS_CONTROL_OPTIONS_KEY = 'ACCESS_CONTROL_OPTIONS';
exports.ACCESS_CONTROL_CTLR_CONFIG_KEY = 'ACCESS_CONTROL_CTLR';
exports.ACCESS_CONTROL_GRANT_CONFIG_KEY = 'ACCESS_CONTROL_GRANTS';
exports.ACCESS_CONTROL_FILTERS_CONFIG_KEY = 'ACCESS_CONTROL_FILTERS';
//# sourceMappingURL=constants.js.map