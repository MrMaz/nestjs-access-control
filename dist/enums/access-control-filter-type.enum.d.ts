export declare enum AccessControlFilterType {
    QUERY = "query",
    BODY = "body",
    PATH = "params"
}
