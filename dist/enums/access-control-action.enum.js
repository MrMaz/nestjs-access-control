"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AccessControlAction;
(function (AccessControlAction) {
    AccessControlAction["CREATE"] = "CREATE";
    AccessControlAction["READ"] = "READ";
    AccessControlAction["UPDATE"] = "UPDATE";
    AccessControlAction["DELETE"] = "DELETE";
})(AccessControlAction = exports.AccessControlAction || (exports.AccessControlAction = {}));
//# sourceMappingURL=access-control-action.enum.js.map