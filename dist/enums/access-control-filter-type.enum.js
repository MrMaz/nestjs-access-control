"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AccessControlFilterType;
(function (AccessControlFilterType) {
    AccessControlFilterType["QUERY"] = "query";
    AccessControlFilterType["BODY"] = "body";
    AccessControlFilterType["PATH"] = "params";
})(AccessControlFilterType = exports.AccessControlFilterType || (exports.AccessControlFilterType = {}));
//# sourceMappingURL=access-control-filter-type.enum.js.map