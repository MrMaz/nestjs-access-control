export declare const InjectAccessControl: () => (target: object, key: string | symbol, index?: number | undefined) => void;
